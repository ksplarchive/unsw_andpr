/**
 * 
 */
package krishagni.aperio.dao.impl.hibernate;

import java.util.Arrays;
import java.util.List;

import krishagni.aperio.model.AperioImageRecord;
import krishagni.aperio.pipeline.impl.ProcessingStatus;
import krishagni.pipeline.dao.DaoException;
import krishagni.pipeline.dao.impl.AbstractDao;
import krishagni.pipeline.dao.impl.QueryParameter;

/**
 * @author Ali Ranalvi
 *
 */
public class AperioImageRecordDao extends AbstractDao<AperioImageRecord> {
	
	private final static String APR_FQN = AperioImageRecord.class.getName();
	private final static String GET_ID_BY_STATUS = APR_FQN + ".getIdByStatus";
	
	private static final List<String> STATUS_LIST = 
		Arrays.asList(ProcessingStatus.NOT_PROCESSED.toString(), ProcessingStatus.ERROR_IN_CATISSUE_UPDATE.toString());
	
	@Override
	public AperioImageRecord getById(Long id) throws DaoException {
		return super.getById(id);	
	}

	@Override
	public List<Long> getIdList() throws DaoException {
		QueryParameter queryParameter = new QueryParameter("statuses", STATUS_LIST); 
		return executeNamedQuery(GET_ID_BY_STATUS, queryParameter);				
	}

	@Override
	public void save(AperioImageRecord aperioImageRecord) throws DaoException {
		super.create(aperioImageRecord);
	}

	@Override
	protected String getEntityName() {
		return APR_FQN;
	}	
}
