/**
 * 
 */
package krishagni.aperio.model;

import java.util.Date;

import krishagni.pipeline.PipelineItem;

/**
 * @author Ali Ranalvi
 *
 */
public class AperioImageRecord extends PipelineItem {
	
	private Long slideId;
	private String barcode;
	private Long digitalImageId;
	private String imageUrl;
	private String description;
	private double qualityFactor;
	private Date scanDate;
	private String scanStatus;
	private String scanStatusDetails;
	private Long width;
	private Long height;
	private String stainName;
	private String imageStatus;
	private String imageComment;
	private String scanScopeId;
	private String magnification;
	private byte[] thumbnail;
	
	/**
	 * @return the slideId
	 */
	public Long getSlideId() {
		return slideId;
	}
	
	/**
	 * @param slideId the slideId to set
	 */
	public void setSlideId(Long slideId) {
		this.slideId = slideId;
	}

	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @param barcode the barcode to set
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * @return the digitalImageId
	 */
	public Long getDigitalImageId() {
		return digitalImageId;
	}

	/**
	 * @param digitalImageId the digitalImageId to set
	 */
	public void setDigitalImageId(Long digitalImageId) {
		this.digitalImageId = digitalImageId;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the qualityFactor
	 */
	public double getQualityFactor() {
		return qualityFactor;
	}

	/**
	 * @param qualityFactor the qualityFactor to set
	 */
	public void setQualityFactor(double qualityFactor) {
		this.qualityFactor = qualityFactor;
	}

	/**
	 * @return the scanDate
	 */
	public Date getScanDate() {
		return scanDate;
	}

	/**
	 * @param scanDate the scanDate to set
	 */
	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}

	/**
	 * @return the scanStatus
	 */
	public String getScanStatus() {
		return scanStatus;
	}

	/**
	 * @param scanStatus the scanStatus to set
	 */
	public void setScanStatus(String scanStatus) {
		this.scanStatus = scanStatus;
	}

	/**
	 * @return the scanStatusDetails
	 */
	public String getScanStatusDetails() {
		return scanStatusDetails;
	}

	/**
	 * @param scanStatusDetails the scanStatusDetails to set
	 */
	public void setScanStatusDetails(String scanStatusDetails) {
		this.scanStatusDetails = scanStatusDetails;
	}

	/**
	 * @return the width
	 */
	public Long getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(Long width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public Long getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(Long height) {
		this.height = height;
	}

	/**
	 * @return the stainName
	 */
	public String getStainName() {
		return stainName;
	}

	/**
	 * @param stainName the stainName to set
	 */
	public void setStainName(String stainName) {
		this.stainName = stainName;
	}

	/**
	 * @return the imageStatus
	 */
	public String getImageStatus() {
		return imageStatus;
	}

	/**
	 * @param imageStatus the imageStatus to set
	 */
	public void setImageStatus(String imageStatus) {
		this.imageStatus = imageStatus;
	}

	/**
	 * @return the imageComment
	 */
	public String getImageComment() {
		return imageComment;
	}

	/**
	 * @param imageComment the imageComment to set
	 */
	public void setImageComment(String imageComment) {
		this.imageComment = imageComment;
	}

	/**
	 * @return the scanScopeId
	 */
	public String getScanScopeId() {
		return scanScopeId;
	}

	/**
	 * @param scanScopeId the scanScopeId to set
	 */
	public void setScanScopeId(String scanScopeId) {
		this.scanScopeId = scanScopeId;
	}

	/**
	 * @return the magnification
	 */
	public String getMagnification() {
		return magnification;
	}

	/**
	 * @param magnification the magnification to set
	 */
	public void setMagnification(String magnification) {
		this.magnification = magnification;
	}

	/**
	 * @return the thumbnail
	 */
	public byte[] getThumbnail() {
		return thumbnail;
	}

	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	

}
