/**
 * 
 */
package krishagni.aperio.pipeline.impl;

/**
 * @author Ali Ranalvi
 *
 */
public enum ProcessingStatus {
	
	NOT_PROCESSED("Record has not yet been processed"), 
	CATISSUE_UPDATED("Record has been updated in caTissue"), 
	ERROR_IN_CATISSUE_UPDATE("Error during update in caTissue"),
	ERROR_IN_CATISSUE_UPDATE_FATAL("Fatal error during update in caTissue");
	
	private String description;

	private ProcessingStatus(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}	
}
