/**
 * 
 */
package krishagni.aperio.pipeline.impl;

import java.net.URI;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import krishagni.aperio.dao.impl.hibernate.AperioImageRecordDao;
import krishagni.aperio.model.AperioImageRecord;
import krishagni.catissueplus.imaging.dto.ImageDTO;
import krishagni.pipeline.Pipeline;
import krishagni.pipeline.PipelineException;
import krishagni.pipeline.PipelineFatalException;
import krishagni.pipeline.dao.Dao;
import krishagni.pipeline.impl.BasePipeline;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * @author Ali Ranalvi
 *
 */
public class CaTissuePipeline extends BasePipeline<AperioImageRecord> {
	
	private static final Logger log = Logger.getLogger(CaTissuePipeline.class);
	
	private static final String APERIO_IMAGE_TYPE = "Aperio";
	private static final String APERIO_EQUIPMENT_PREFIX = "APERIO_";
	private static final String PROPERTIES_FILENAME = "/pipeline.properties";
	
	private static final String PROP_CTPLUS_RESTAPI_URL = "catissueplus.restapi.url";
	private static final String PROP_CTPLUS_RESTAPI_USERNAME = "catissueplus.restapi.username";
	private static final String PROP_CTPLUS_RESTAPI_PASSWORD = "catissueplus.restapi.password";
	
	private WebResource service;
	
	private String restApiUrl;
	private String restApiUsr;
	private String restApiPwd;
	
	public CaTissuePipeline(Dao<AperioImageRecord> dao) throws Exception {
		super(dao);
		loadProperties();
		ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(config);
		client.addFilter(new com.sun.jersey.api.client.filter.HTTPBasicAuthFilter(this.restApiUsr, this.restApiPwd));
		service = client.resource(getBaseURI());
	}
	
	private void loadProperties() throws Exception {
		Properties properties = new Properties();
		properties.load(this.getClass().getResourceAsStream(PROPERTIES_FILENAME));
		if (!properties.containsKey(PROP_CTPLUS_RESTAPI_URL)) {
			log.error("Application property '" + PROP_CTPLUS_RESTAPI_URL + "' not defined");
			throw new Exception("Application property '" + PROP_CTPLUS_RESTAPI_URL + "' not defined");
		}
		if (!properties.containsKey(PROP_CTPLUS_RESTAPI_USERNAME)) {
			log.error("Application property '" + PROP_CTPLUS_RESTAPI_USERNAME + "' not defined");
			throw new Exception("Application property '" + PROP_CTPLUS_RESTAPI_USERNAME + "' not defined");
		}
		if (!properties.containsKey(PROP_CTPLUS_RESTAPI_PASSWORD)) {
			log.error("Application property '" + PROP_CTPLUS_RESTAPI_PASSWORD + "' not defined");
			throw new Exception("Application property '" + PROP_CTPLUS_RESTAPI_PASSWORD + "' not defined");
		}		
		this.restApiUrl = properties.getProperty(PROP_CTPLUS_RESTAPI_URL);
		this.restApiUsr = properties.getProperty(PROP_CTPLUS_RESTAPI_USERNAME);
		this.restApiPwd = properties.getProperty(PROP_CTPLUS_RESTAPI_PASSWORD);
	}
	
	private URI getBaseURI() {
		return UriBuilder.fromUri(this.restApiUrl).build();
	}
	
	private ImageDTO makeDTO(AperioImageRecord aperioImageRecord) {
		ImageDTO imageDTO = new ImageDTO();
		imageDTO.setSpecimenBarcode(aperioImageRecord.getBarcode());
		imageDTO.setEquipmentImageId(aperioImageRecord.getDigitalImageId().toString());
		imageDTO.setEquipmentId(APERIO_EQUIPMENT_PREFIX + aperioImageRecord.getScanScopeId());		
		imageDTO.setDescription(aperioImageRecord.getDescription());
		imageDTO.setHeight(aperioImageRecord.getHeight());
		imageDTO.setWidth(aperioImageRecord.getWidth());
		imageDTO.setQuality(aperioImageRecord.getQualityFactor());
		imageDTO.setResolution(getResolution(aperioImageRecord.getMagnification()));
		imageDTO.setScanDate(aperioImageRecord.getScanDate());		
		imageDTO.setStatus(aperioImageRecord.getImageStatus());		
		imageDTO.setStainName(aperioImageRecord.getStainName());
		imageDTO.setThumbnail(aperioImageRecord.getThumbnail());
		imageDTO.setImageType(APERIO_IMAGE_TYPE);
		return imageDTO;		
	}

	private Long getResolution(String magnification) {
		return (magnification != null && magnification.endsWith("x")) ?
				Long.parseLong(magnification.substring(0, magnification.length() - 1)) : null;
	}
	
	@Override
	public String getFailureStatus() {
		return ProcessingStatus.ERROR_IN_CATISSUE_UPDATE.toString();
	}

	@Override
	public String getFatalFailureStatus() {
		return ProcessingStatus.ERROR_IN_CATISSUE_UPDATE_FATAL.toString();
	}

	@Override
	public String getSuccessStatus() {
		return ProcessingStatus.CATISSUE_UPDATED.toString();
	}

	@Override
	public void process(AperioImageRecord aperioImageRecord) throws PipelineException, PipelineFatalException {
		ImageDTO imageDTO = makeDTO(aperioImageRecord);		
		ClientResponse response = service.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, imageDTO);
		if (response != null) {
			 Status responseStatus = response.getClientResponseStatus();
			 switch (responseStatus) {
			 case OK:
				 log.info("AperioImageRecord#" + aperioImageRecord.getDigitalImageId() + " processed successfully: " + response.getStatus() + ": " + response.getEntity(String.class));
				 break;
			 case INTERNAL_SERVER_ERROR:				 
			 case SERVICE_UNAVAILABLE:
			 case BAD_GATEWAY:
				 log.error("caTissue REST API unreachable, error: " + response.getEntity(String.class));
				 throw new PipelineException("caTissue REST API unreachable, error: " + response.getEntity(String.class));
			 case UNAUTHORIZED:
				 log.error("Authentication failed for " + this.restApiUsr + " - please examine credentials in " + PROPERTIES_FILENAME + ", error: " + response.getEntity(String.class));
				 throw new PipelineException("Authentication failed for " + this.restApiUsr + " - please examine credentials in " + PROPERTIES_FILENAME + ", error: " + response.getEntity(String.class));
			 case FORBIDDEN:
				 log.error("Authorization failed for "  + this.restApiUsr + " - please examine credentials in " + PROPERTIES_FILENAME + ", error: " + response.getEntity(String.class));
				 throw new PipelineException("Authorization failed for "  + this.restApiUsr + " - please examine credentials in " + PROPERTIES_FILENAME + ", error: " + response.getEntity(String.class));
			 case BAD_REQUEST:
			 case NOT_ACCEPTABLE:
				 log.error("AperioImageRecord contents led to error on server, error: " + response.getEntity(String.class));
				 throw new PipelineFatalException("AperioImageRecord contents led to error on server, error: " + response.getEntity(String.class));
			 case METHOD_NOT_ALLOWED:
			 case REQUEST_TIMEOUT:
			 case UNSUPPORTED_MEDIA_TYPE:
			 case NOT_FOUND:
				 log.error("caTissue REST API not functioning as per specification, error: " + response.getEntity(String.class));
				 throw new PipelineFatalException("caTissue REST API not functioning as per specification, error: " + response.getEntity(String.class));
			 }
		} else {
			log.error("caTissue REST API unreachable, null response");
			throw new PipelineException("caTissue REST API unreachable, null response");
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AperioImageRecordDao dao = new AperioImageRecordDao();
			Pipeline<AperioImageRecord> pipeline = new CaTissuePipeline(dao);
			log.info("Starting " + CaTissuePipeline.class.getName() + "...");
			new Thread(pipeline).start();
			log.info("Completed execution of " + CaTissuePipeline.class.getName());
		} catch (Exception e) {
			log.error("Error running " + CaTissuePipeline.class.getName() + ": " + e.getMessage());
		}		
	}	
}
