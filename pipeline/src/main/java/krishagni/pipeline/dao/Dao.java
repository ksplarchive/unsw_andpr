package krishagni.pipeline.dao;

import java.util.List;

import krishagni.pipeline.PipelineItem;

/**
 * @author Ali Ranalvi
 *
 */
public interface Dao<T extends PipelineItem> {
	
	public List<Long> getIdList() throws DaoException;
	
	public T getById(Long id) throws DaoException;
	
	public void save(T item) throws DaoException;

}
