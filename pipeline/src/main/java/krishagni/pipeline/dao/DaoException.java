/**
 * 
 */
package krishagni.pipeline.dao;

/**
 * @author Ali Ranalvi
 *
 */
public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2081205322584981108L;
	
	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(Exception e) {
		super(e);
	}
	
	public DaoException(String message, Exception e) {
		super(message, e);
	}

}
