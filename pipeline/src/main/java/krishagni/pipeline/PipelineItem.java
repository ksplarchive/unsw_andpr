/**
 * 
 */
package krishagni.pipeline;

import java.util.Date;

/**
 * @author Ali Ranalvi
 *
 */
public class PipelineItem {
	
	private Long identifier;	
	private String status;
	private String statusDesc;
	private Date lastProcessedDate;
	
	/**
	 * @return the identifier
	 */
	public Long getIdentifier() {
		return identifier;
	}
	
	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusDesc
	 */
	public String getStatusDesc() {
		return statusDesc;
	}

	/**
	 * @param statusDesc the statusDesc to set
	 */
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	/**
	 * @return the lastProcessedDate
	 */
	public Date getLastProcessedDate() {
		return lastProcessedDate;
	}

	/**
	 * @param lastProcessedDate the lastProcessedDate to set
	 */
	public void setLastProcessedDate(Date lastProcessedDate) {
		this.lastProcessedDate = lastProcessedDate;
	}

}
