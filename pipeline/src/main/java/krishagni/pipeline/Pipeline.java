/**
 * 
 */
package krishagni.pipeline;


/**
 * @author Ali Ranalvi
 *
 */
public interface Pipeline<T extends PipelineItem> extends Runnable {
		
	public void process(T entry) throws PipelineException, PipelineFatalException;
	
	public String getSuccessStatus();
	
	public String getFailureStatus();
	
	public String getFatalFailureStatus();

}
