/**
 * 
 */
package krishagni.pipeline;

/**
 * @author Ali Ranalvi
 *
 */
public class PipelineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6619675031130528864L;
	
	public PipelineException(String message) {
		super(message);
	}
	
	public PipelineException(Exception e) {
		super(e);
	}
	
	public PipelineException(String message, Exception e) {
		super(message, e);
	}	

}
