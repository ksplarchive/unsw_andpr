/**
 * 
 */
package krishagni.pipeline;

/**
 * @author Ali Ranalvi
 *
 */
public class PipelineFatalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2762772014905873292L;
	
	public PipelineFatalException(String message) {
		super(message);
	}
	
	public PipelineFatalException(Exception e) {
		super(e);
	}
	
	public PipelineFatalException(String message, Exception e) {
		super(message, e);
	}
	
}
