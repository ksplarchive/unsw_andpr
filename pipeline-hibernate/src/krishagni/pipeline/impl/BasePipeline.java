/**
 * 
 */
package krishagni.pipeline.impl;

import java.util.Calendar;
import java.util.List;

import krishagni.pipeline.Pipeline;
import krishagni.pipeline.PipelineException;
import krishagni.pipeline.PipelineFatalException;
import krishagni.pipeline.PipelineItem;
import krishagni.pipeline.dao.Dao;
import krishagni.pipeline.dao.DaoException;
import krishagni.pipeline.dao.impl.HibernateUtil;

import org.apache.log4j.Logger;


/**
 * @author Ali Ranalvi
 * @param <T>
 *
 */
public abstract class BasePipeline<T extends PipelineItem> implements Pipeline<T> {

	private static final Logger log = Logger.getLogger(BasePipeline.class);

	private Dao<T> dao;

	public BasePipeline(Dao<T> dao) {
		this.dao = dao;
	}
	
	private List<Long> getIdList() throws DaoException {
		try {
			HibernateUtil.beginTxn();
			List<Long> idList = dao.getIdList();
			HibernateUtil.commitTxn();
			return idList;
		} finally {
			HibernateUtil.closeCurrentSession();
		}
	}
	
	private T getById(Long id) throws DaoException {
		try {
			HibernateUtil.beginTxn();
			T item = dao.getById(id);
			HibernateUtil.commitTxn();
			return item;
		} finally {
			HibernateUtil.closeCurrentSession();
		}
	}
	
	private void save(T item) throws DaoException {
		try {
			HibernateUtil.beginTxn();
			dao.save(item);
			HibernateUtil.commitTxn();
		} finally {
			HibernateUtil.closeCurrentSession();
		}
	}


	@Override
	public void run() {
		try {
			List<Long> idList = getIdList();
			if (idList == null || idList.size() == 0) {
				log.info("No candidate records found for pipeline processing");
				return;
			}			
			
			log.info("Candidate records found for pipeline processing = " + idList.size());

			for (Long id : idList) {
				T item = getById(id);
				try {					
					process(item);
					item.setStatus(getSuccessStatus());
					item.setStatusDesc(getSuccessStatus());
				} catch (PipelineException pe) {					
					log.error("Processing for record#" + id + " failed: " + pe.getMessage());
					pe.printStackTrace();
					item.setStatus(getFailureStatus());
					item.setStatusDesc(pe.getMessage());
				} catch (PipelineFatalException pfe) {
					log.error("Processing for record#" + id + " failed fatally: " + pfe.getMessage());
					pfe.printStackTrace();
					item.setStatus(getFatalFailureStatus());
					item.setStatusDesc(pfe.getMessage());
				} 
				item.setLastProcessedDate(Calendar.getInstance().getTime());
				save(item);
			}
		} catch (DaoException e) {
			e.printStackTrace();
			log.fatal("Pipeline failed due to error in DAO: " + e.getMessage() + ", aborting");
		}
	}
}
