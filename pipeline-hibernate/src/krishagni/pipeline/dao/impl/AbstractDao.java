/**
 * 
 */
package krishagni.pipeline.dao.impl;

import java.util.Collection;
import java.util.List;

import krishagni.pipeline.PipelineItem;
import krishagni.pipeline.dao.Dao;
import krishagni.pipeline.dao.DaoException;

import org.hibernate.Query;
import org.hibernate.Session;


/**
 * @author Vinayak Pawar
 *
 */
public abstract class AbstractDao<T extends PipelineItem> implements Dao<T> {

	public void create(T entity) throws DaoException {
		try {
			HibernateUtil.getCurrentSession().saveOrUpdate(getEntityName(), entity);
		} catch (Exception e) {
			throw new DaoException("Failed to create the entity in DB", e);
		}
	}

	public void update(T entity) throws DaoException {
		try {
			HibernateUtil.getCurrentSession().saveOrUpdate(getEntityName(), entity);
		} catch (Exception e) {
			throw new DaoException("Failed to update the entity in DB", e);
		}
	}

	public void delete(T entity) throws DaoException {
		try {
			HibernateUtil.getCurrentSession().delete(getEntityName(), entity);
		} catch (Exception e) {
			throw new DaoException("Failed to update the entity in DB", e);
		}
	}

	/* (non-Javadoc)
	 * @see com.indemandsoft.cleanser.dao.Dao#getById(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T getById(Long identifier) throws DaoException {
		try {
			return (T)HibernateUtil.getCurrentSession().get(getEntityName(), identifier);			
		} catch (Exception e) {
			throw new DaoException("Failed to retrieve entity from DB", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <U> List<U> executeNamedQuery(String queryName, QueryParameter ... parameters)
	throws DaoException {
		try {
			Session session = HibernateUtil.getCurrentSession();
			Query queryObj = session.getNamedQuery(queryName);
			populateParameters(queryObj, parameters);
			return (List<U>)queryObj.list();
		} catch (Exception e) {
			throw new DaoException("Error executing the named query: " + queryName, e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void populateParameters(Query query, QueryParameter ... parameters) {
		if (parameters != null) {
			for (QueryParameter parameter : parameters) {
				if (parameter.getValue() instanceof Collection) {
					query.setParameterList(parameter.getName(), (Collection)parameter.getValue());
				} else {
					query.setParameter(parameter.getName(), parameter.getValue());
				}
				
			}
		}				
	}
	
	protected abstract String getEntityName();

}