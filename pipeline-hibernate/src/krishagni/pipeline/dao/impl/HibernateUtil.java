/**
 * 
 */
package krishagni.pipeline.dao.impl;

import java.net.URL;
import java.util.Properties;

import krishagni.pipeline.dao.DaoException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * @author Vinayak Pawar
 *
 */
public class HibernateUtil {

	private static SessionFactory sessionFactory = null;

	private static class SessionFactoryHolder {		
		private static final SessionFactory sessionFactory;

		static {			
			try {
				String cfg = "/hibernate.cfg.xml";
				URL cfgUrl = SessionFactoryHolder.class.getResource(cfg);
				Properties properties = new Properties();
				properties.load(SessionFactoryHolder.class.getResourceAsStream("/db.properties"));
				Configuration configuration = new Configuration();			
				sessionFactory = configuration.setProperties(properties).configure(cfgUrl).buildSessionFactory();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				throw new ExceptionInInitializerError(e);
			}			
		}
	}


	public static void setSessionFactory(SessionFactory sessionFactory) {
		HibernateUtil.sessionFactory = sessionFactory;
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = SessionFactoryHolder.sessionFactory; 
		}		
		return sessionFactory;
	}

	public static void beginTxn() throws DaoException {
		try {
			getCurrentSession().beginTransaction();
		} catch (HibernateException hbe) {
			throw new DaoException(hbe);
		}
	}

	public static void commitTxn() throws DaoException {
		try {
			Transaction txn = getCurrentSession().getTransaction();
			if (txn == null) {
				throw new DaoException("No open transaction to commit");
			}
			txn.commit();
		} catch (HibernateException hbe) {
			throw new DaoException(hbe);
		} 
	}

	public static void rollbackTxn() throws DaoException {
		try {
			Transaction txn = getCurrentSession().getTransaction();
			if (txn == null) {
				throw new DaoException("No open transaction to rollback");
			}
			txn.rollback();            
		} catch (HibernateException hbe) {
			throw new DaoException(hbe);
		}
	}

	public static void closeCurrentSession() throws DaoException {
		try {
			getCurrentSession().close();
		} catch (HibernateException hbe) {
			throw new DaoException(hbe);
		}        
	}

	public static Session getCurrentSession() {
		return HibernateUtil.getSessionFactory().getCurrentSession();
	}
}
