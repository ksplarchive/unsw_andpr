package edu.unsw.andpr;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class TestServletListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.err.println("---------- Loading Image Class ---------");
		try {
			Class.forName("edu.unsw.andpr.api.ImageService");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

}
