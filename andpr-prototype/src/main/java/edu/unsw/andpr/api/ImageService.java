/**
 * 
 */
package edu.unsw.andpr.api;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import edu.unsw.andpr.dao.ImageDao;
import edu.unsw.andpr.dao.InstrumentDao;
import edu.unsw.andpr.dao.LocationDao;
import edu.unsw.andpr.dao.ParticipantDao;
import edu.unsw.andpr.dao.ServerDao;
import edu.unsw.andpr.dao.SpecimenDao;
import edu.unsw.andpr.dao.UserDao;
import edu.unsw.andpr.domain.Image;
import edu.unsw.andpr.domain.ImageStat;
import edu.unsw.andpr.domain.ImageSummary;
import edu.unsw.andpr.domain.Instrument;
import edu.unsw.andpr.domain.Location;
import edu.unsw.andpr.domain.Participant;
import edu.unsw.andpr.domain.Server;
import edu.unsw.andpr.domain.Specimen;
import edu.unsw.andpr.domain.TiledImage;
import edu.unsw.andpr.domain.User;
import edu.unsw.andpr.util.BeanFactory;
import edu.unsw.andpr.util.ImageXmlParser;


/**
 * @author Vinayak Pawar
 *
 */

@Path("image")
public class ImageService {
	private static final Logger logger = Logger.getLogger(ImageService.class); 
	
	@GET
	@Path("stat")
	@Produces({MediaType.APPLICATION_JSON})
	public Response stats() {				
		ImageDao imageDao = BeanFactory.getImageDao();
		
		ImageStat imageStat = new ImageStat();
		imageStat.setWsiCnt(imageDao.getImagesCount());
		imageStat.setTiledImagesCnt(imageDao.getTiledImagesCount());
		imageStat.setLastWeekWsiUploadCnt(imageDao.getImagesCount(7));
		imageStat.setLastWeekTiledImagesUploadCnt(imageDao.getTiledImagesCount(7));
		
		return Response.ok(imageStat, MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({MediaType.APPLICATION_JSON})
	public Response uploadImage(MultipartFormDataInput input) {
		Map<String, List<InputPart>> formData = input.getFormDataMap();
		
		try {
			String xml = getXml(formData);
			Long userId = getUserId(formData);
			logger.info("The XML supplied by user ID: " + userId + " is\n" + xml);
			
			Image image = ImageXmlParser.createImage(xml);
			setImageSpecimen(image);
			setImageParticipant(image);
			setImageInstrument(image);

			Date now = Calendar.getInstance().getTime();
			User user = new User();
			user.setIdentifier(userId);
			image.setCreatedBy(user);
			image.setLastUpdatedBy(user);
			image.setUpdateTime(now);
			
			Map<String, Long> serverNameIds = new HashMap<String, Long>();
			setImageLocation(image, serverNameIds);						
			for (TiledImage tiledImage : image.getTiledImages()) {
				setTiledImageLocation(tiledImage, serverNameIds);
				tiledImage.setUpdateTime(now);
			}
			
			ImageDao imageDao = BeanFactory.getImageDao();
			imageDao.insert(image);
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("imageId", image.getIdentifier());
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (IllegalArgumentException iae) {
			logger.info("Illegal arguments supplied by user", iae);
			
			Map<String, Object> message = new HashMap<String, Object>();
			message.put("statusMessage", iae.getMessage());
			return Response.status(Status.BAD_REQUEST).entity(message).build();
		} catch (Exception e) {
			logger.error("Error upload image XML", e);
			
			Map<String, Object> message = new HashMap<String, Object>();
			message.put("statusMessage", e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(message).build();
		}		
	}
	
	@GET
	@Path("{imageid}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getImage(@PathParam("imageid") Long imageId) {
		Image image = BeanFactory.getImageDao().getImageById(imageId);
		if (image == null) {
			Map<String, Object> message = new HashMap<String, Object>();
			message.put("statusMessage", "Image (id = " + imageId + ") does not exists");
			return Response.status(Status.NOT_FOUND).entity(message).build();
		}
				
		UserDao userDao = BeanFactory.getUserDao();
		if (image.getCreatedBy() != null) {
			image.setCreatedBy(userDao.getUserById(image.getCreatedBy().getIdentifier()));
		}
		
		if (image.getLastUpdatedBy() != null) {
			image.setLastUpdatedBy(userDao.getUserById(image.getLastUpdatedBy().getIdentifier()));
		}
		
		if (image.getSpecimen() != null) {
			image.setSpecimen(BeanFactory.getSpecimenDao()
					.getSpecimenById(image.getSpecimen().getIdentifier()));
		}
		
		if (image.getParticipant() != null) {
			image.setParticipant(BeanFactory.getParticipantDao()
					.getParticipantById(image.getParticipant().getIdentifier()));
		}
		
		image.setGeneratedBy(BeanFactory.getInstrumentDao()
				.getInstrumentById(image.getGeneratedBy().getIdentifier()));
		
		LocationDao locationDao = BeanFactory.getLocationDao();
		Location location = locationDao.getLocationById(image.getStoredAt().getIdentifier());
		image.setStoredAt(location);
		
		for (TiledImage tiledImage : image.getTiledImages()) {
			location = locationDao.getLocationById(tiledImage.getLocation().getIdentifier());
			tiledImage.setLocation(location);
		}
		
		return Response.ok(image, MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getImage(@Context HttpServletRequest req) {
		try {
			int pageNum = getInteger(req.getParameter("page"), 1);
			int numPageRecs = getInteger(req.getParameter("rows"), 10);
			int startRow = 0;
			
			if (pageNum > 0) {
				startRow = (pageNum - 1) * numPageRecs;
			}

			logger.info("Page num: " + pageNum + ", page recs: " + numPageRecs + ", start row: " + startRow);		
			
			List<ImageSummary> images = new ArrayList<ImageSummary>();
			ImageDao imageDao = BeanFactory.getImageDao();
			
			String searchEntity = req.getParameter("searchEntity");			
			logger.info("Search entity: " + searchEntity);			
			if (searchEntity == null || searchEntity.isEmpty()) {
				images = imageDao.getAllImages(startRow, numPageRecs);
			} else if (searchEntity.equals("participant")) {
				String name = req.getParameter("name");
				if (name == null || name.trim().isEmpty()) {
					throw new IllegalArgumentException("Search parameter name is either null or empty");
				}
				
				images = imageDao.getImagesByParticipantName(name, startRow, numPageRecs);
			} else if (searchEntity.equals("specimen")) {
				String barcode = req.getParameter("barcode");
				if (barcode == null || barcode.trim().isEmpty()) {
					throw new IllegalArgumentException("Search parameter barcode is either null or empty");
				}
				
				images = imageDao.getImagesBySpecimenBarcode(barcode, startRow, numPageRecs);
			} else if (searchEntity.equals("instrument")) {
				String manufacturer = req.getParameter("manufacturer");
				if (manufacturer == null || manufacturer.trim().isEmpty()) {
					throw new IllegalArgumentException("Search parameter manufacturer is either null or empty");
				}
				
				images = imageDao.getImagesByInstrumentManufacturer(manufacturer, startRow, numPageRecs);
			} else if (searchEntity.equals("tiledImage")) {
				String xStr = req.getParameter("x");
				String yStr = req.getParameter("y");
				
				if (xStr == null || xStr.isEmpty() || yStr == null || yStr.isEmpty()) {
					throw new IllegalArgumentException("Search parameters (x,y) are either null or empty");
				}
				
				int x, y;
				try {
					x = Integer.parseInt(xStr);
					y = Integer.parseInt(yStr);
				} catch (NumberFormatException nfe) {
					throw new IllegalArgumentException("Search parameters (x,y) are invalid");
				}
				
				images = imageDao.getImagesByTileCoordinates(x, y, startRow, numPageRecs);
			} else if (searchEntity.equals("updateTime")) {
				String nDaysStr = req.getParameter("days");
				if (nDaysStr == null || nDaysStr.isEmpty()) {
					throw new IllegalArgumentException("Search parameter days is either null or empty");
				}
				
				int nDays;
				try {
					nDays = Integer.parseInt(nDaysStr);
				} catch (NumberFormatException nfe) {
					throw new IllegalArgumentException("Search parameter days is invalid");
				}
				
				images = imageDao.getImagesUploadedInLast(nDays);
			} else {
				throw new IllegalArgumentException("Invalid search entity " + searchEntity);
			}
			
			return Response.ok(images, MediaType.APPLICATION_JSON).build();			
		} catch (IllegalArgumentException iae) {
			Map<String, String> message = new HashMap<String, String>();
			message.put("statusMessage", iae.getMessage());
			return Response.status(Status.BAD_REQUEST).entity(message).build();
		}
	}
	
	private String getXml(Map<String, List<InputPart>> formData) {
		List<InputPart> inputParts = formData.get("imageXml");
		
		if (inputParts == null || inputParts.size() != 1) {
			throw new IllegalArgumentException("No input image XML");
		}
		
		try {
			InputPart xmlFileInput = inputParts.get(0);
			InputStream xmlStream = xmlFileInput.getBody(InputStream.class, null);
			
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			byte[] block = new byte[4096];
			int numRead;
			
			while ((numRead = xmlStream.read(block, 0, 4096)) > 0) {
				byteStream.write(block, 0, numRead);
			}
			
			return new String(byteStream.toByteArray());
		} catch (Exception e) {
			throw new RuntimeException("Error reading input xml", e);
		}		
	}
	
	private Long getUserId(Map<String, List<InputPart>> formData) {
		List<InputPart> inputParts = formData.get("userId");
		
		if (inputParts == null || inputParts.size() != 1) {
			throw new IllegalArgumentException("Either no user id is supplied or many user ids are supplied");
		}
		
		try {
			String userIdStr = inputParts.get(0).getBody(String.class, null);
			return Long.parseLong(userIdStr);
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException("Invalid user id");
		} catch (Exception e) {
			throw new RuntimeException("Error obtaining user id", e);
		}		
	}
	
	private int getInteger(String valueStr, int defaultVal) {
		int retVal = defaultVal;
		
		if (valueStr != null) {
			try {
				retVal = Integer.parseInt(valueStr);
			} catch (NumberFormatException nfe) {
				retVal = defaultVal;
			}
		}
		
		return retVal;		
	}
	
	private void setImageSpecimen(Image image) {
		Specimen specimen = image.getSpecimen();
		if (specimen == null) {
			return;
		}
		
		String barcode = specimen.getBarcode();
		if (barcode == null) {
			throw new IllegalArgumentException("Specimen barcode can not be null");
		}
			
		SpecimenDao specimenDao = BeanFactory.getSpecimenDao();
		Long specimenId = specimenDao.getSpecimenIdByBarcode(barcode);
		if (specimenId == -1) {
			specimenDao.insert(specimen);
		} else {
			specimen.setIdentifier(specimenId);
		}		
	}
	
	private void setImageParticipant(Image image) {
		Participant participant = image.getParticipant();
		if (participant == null) {
			return;			
		}
		
		ParticipantDao participantDao = BeanFactory.getParticipantDao();
		if (participant.getIdentifier() == null) {
			participantDao.insert(participant);
		} else if (!participantDao.doesParticipantExists(participant.getIdentifier())) {
			throw new IllegalArgumentException("Invalid participant ID (" + participant.getIdentifier() + ")");
		}
	}
	
	private void setImageInstrument(Image image) {
		Instrument instrument = image.getGeneratedBy();
				
		Long instrumentId = instrument.getIdentifier();
		if (instrumentId == null) {
			throw new IllegalArgumentException("Image instrument ID can not be null");
		}
		
		InstrumentDao instrumentDao = BeanFactory.getInstrumentDao();
		if (!instrumentDao.doesInstrumentExists(instrumentId)) {
			throw new IllegalArgumentException("Invalid instrument ID (" + instrumentId + ")");
		} 
	}
	
	private void setImageLocation(Image image, Map<String, Long> serverIdsMap) {
		Location location = image.getStoredAt();
		setImageLocation(location, serverIdsMap);
	}
	
	private void setTiledImageLocation(TiledImage tiledImage, Map<String, Long> serverIdsMap) {
		Location location = tiledImage.getLocation();
		setImageLocation(location, serverIdsMap);		
	}
	
	private void setImageLocation(Location location, Map<String, Long> serverIdsMap) {
		if (location == null) {
			throw new IllegalArgumentException("Image location can not be null");
		}
		
		Server server = location.getServer();
		if (server == null) {
			throw new IllegalArgumentException("Image location server can not be null");
		}
		
		String serverName = server.getName();
		if (serverName == null) {
			throw new IllegalArgumentException("Image location server name can not be null");
		}
		
		Long serverId = serverIdsMap.get(serverName);
		if (serverId == null) {
			ServerDao serverDao = BeanFactory.getServerDao();
			serverId = serverDao.getServerId(serverName);
		}
		
		if (serverId == -1) {
			throw new IllegalArgumentException("Invalid image location server name (" + serverName + ")");
		}
		
		server.setIdentifier(serverId);
		serverIdsMap.put(serverName, serverId);
		
		LocationDao locationDao = BeanFactory.getLocationDao();
		locationDao.insert(location);		
	}
}
