package edu.unsw.andpr.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.unsw.andpr.dao.UserDao;
import edu.unsw.andpr.domain.User;
import edu.unsw.andpr.util.BeanFactory;

@Path("login")
public class LoginService {
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response authenticate(@Context HttpServletRequest req) {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		UserDao userDao = BeanFactory.getUserDao();
		User user = userDao.getUserByLoginId(username);
		
		if (password == null || user == null || !user.getPasswd().equals(password)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("userId", user.getIdentifier());
		response.put("firstName", user.getFirstName());
		response.put("lastName", user.getLastName());
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
}