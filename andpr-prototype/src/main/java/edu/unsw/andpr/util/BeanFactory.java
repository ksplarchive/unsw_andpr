package edu.unsw.andpr.util;

import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.beans.factory.access.SingletonBeanFactoryLocator;

import edu.unsw.andpr.dao.ImageDao;
import edu.unsw.andpr.dao.InstrumentDao;
import edu.unsw.andpr.dao.LocationDao;
import edu.unsw.andpr.dao.ParticipantDao;
import edu.unsw.andpr.dao.ServerDao;
import edu.unsw.andpr.dao.SpecimenDao;
import edu.unsw.andpr.dao.UserDao;

public class BeanFactory {
	private static final String BEAN_FACTORY = "andprContext";
	
	public static BeanFactoryReference bfr = getBeanFactoryReference();
			
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) {
		return (T)bfr.getFactory().getBean(name);
	}

	
	public static UserDao getUserDao() {
		return getBean("userDao");
	}
	
	public static ImageDao getImageDao() {
		return getBean("imageDao");
	}
	
	public static InstrumentDao getInstrumentDao() {
		return getBean("instrumentDao");
	}
	
	public static LocationDao getLocationDao() {
		return getBean("locationDao");
	}	

	public static ParticipantDao getParticipantDao() {
		return getBean("participantDao");
	}
	
	public static ServerDao getServerDao() {
		return getBean("serverDao");
	}
	
	public static SpecimenDao getSpecimenDao() {
		return getBean("specimenDao");
	}		
	
	private static BeanFactoryReference getBeanFactoryReference() {
		BeanFactoryLocator bfl = SingletonBeanFactoryLocator.getInstance();
		return bfl.useBeanFactory(BEAN_FACTORY);
	}
	
}
