package edu.unsw.andpr.util;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.AbstractRulesModule;
import org.apache.commons.digester3.binder.DigesterLoader;

import edu.unsw.andpr.domain.Image;
import edu.unsw.andpr.domain.Location;
import edu.unsw.andpr.domain.Participant;
import edu.unsw.andpr.domain.Specimen;
import edu.unsw.andpr.domain.TiledImage;

public class ImageXmlParser {
	
	private static final DigesterLoader digesterLoader = DigesterLoader.newLoader(new ImageXmlRulesModule());
	
	public static Image createImage(String xml) {
		try {
			Digester digester = digesterLoader.newDigester();
			return digester.parse(new ByteArrayInputStream(xml.getBytes()));			
		} catch (Exception e) {
			throw new RuntimeException("Error parsing input XML", e);
		}
	}
	
	
	public static class ImageXmlRulesModule extends AbstractRulesModule {

		@Override
		protected void configure() {
			forPattern("image").createObject().ofType(Image.class)
				.then().setProperties();
			
			forPattern("image/name").setBeanProperty();
			forPattern("image/acquisition-date")
				.callMethod("setAcquisitionDateInSecs").withParamCount(1)
				.withParamTypes(Long.class)
				.then().callParam();
			forPattern("image/archived").setBeanProperty();
			forPattern("image/description").setBeanProperty();
			forPattern("image/format").setBeanProperty();
			forPattern("image/file-size").setBeanProperty().withName("fileSize");
			forPattern("image/resolution").setBeanProperty();
			forPattern("image/label").setBeanProperty();
			forPattern("image/activity-status").setBeanProperty().withName("activityStatus");
			forPattern("image/created-by")
				.callMethod("setCreatedByUser").withParamCount(1)
				.withParamTypes(Long.class)
				.then().callParam();
			forPattern("image/equipment-id")
				.callMethod("setGeneratedByEquipment").withParamCount(1)
				.withParamTypes(Long.class)
				.then().callParam();
			
			forPattern("image/specimen").createObject().ofType(Specimen.class)
				.then().setProperties()
				.then().setNext("setSpecimen");
			forPattern("image/specimen/barcode").setBeanProperty();
			forPattern("image/specimen/lineage").setBeanProperty();
			forPattern("image/specimen/class").setBeanProperty().withName("specimenClass");
			forPattern("image/specimen/type").setBeanProperty().withName("specimenType");
			forPattern("image/specimen/tissue-side").setBeanProperty().withName("tissueSide");
			forPattern("image/specimen/tissue-site").setBeanProperty().withName("tissueSite");
			
			forPattern("image/participant").createObject().ofType(Participant.class)
				.then().setProperties()
				.then().setNext("setParticipant");
			forPattern("image/participant/identifier").setBeanProperty();
			forPattern("image/participant/first-name").setBeanProperty().withName("firstName");
			forPattern("image/participant/last-name").setBeanProperty().withName("lastName");
			forPattern("image/participant/date-of-birth")
				.callMethod("setDateOfBirthInSecs").withParamCount(1)
				.withParamTypes(Long.class)
				.then().callParam();
			
			forPattern("image/location").createObject().ofType(Location.class)
				.then().setProperties()
				.then().setNext("setStoredAt");
			forPattern("image/location/folder").setBeanProperty();
			forPattern("image/location/server")
				.callMethod("setServerName").withParamCount(1)
				.withParamTypes(String.class)
				.then().callParam();
			
			forPattern("image/tiled-images/tiled-image").createObject().ofType(TiledImage.class)
				.then().setProperties()
				.then().setNext("addTiledImage");
			forPattern("image/tiled-images/tiled-image/x-cord")
				.setBeanProperty().withName("x");
			forPattern("image/tiled-images/tiled-image/y-cord")
				.setBeanProperty().withName("y");			
			forPattern("image/tiled-images/tiled-image/height").setBeanProperty();
			forPattern("image/tiled-images/tiled-image/width").setBeanProperty();
			forPattern("image/tiled-images/tiled-image/file-size")
				.setBeanProperty().withName("fileSize");
			forPattern("image/tiled-images/tiled-image/resolution").setBeanProperty();
			forPattern("image/tiled-images/tiled-image/tiled-date")
				.callMethod("setTiledDateInSecs").withParamCount(1)
				.withParamTypes(Long.class)
				.then().callParam();
			forPattern("image/tiled-images/tiled-image/format").setBeanProperty();
			
			forPattern("image/tiled-images/tiled-image/location")
				.createObject().ofType(Location.class)
				.then().setProperties()
				.then().setNext("setLocation");
			
			forPattern("image/tiled-images/tiled-image/location/folder")
				.setBeanProperty();			
			forPattern("image/tiled-images/tiled-image/location/server")
				.callMethod("setServerName").withParamCount(1)
				.withParamTypes(String.class)
				.then().callParam();			
		}		
	}

}
