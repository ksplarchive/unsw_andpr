package edu.unsw.andpr.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.LocationDao;
import edu.unsw.andpr.domain.Location;
import edu.unsw.andpr.domain.Server;

public class JdbcLocationDao implements LocationDao {
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;	

	private String SQL_INSERT_LOCATION = "insert into LOCATIONS values (default, :folder, :server_id)";
	
	private String SQL_GET_LOCATION_BY_ID = 
			"select l.identifier, l.folder, l.server_id, s.host_name, s.ip_address, s.name " +
			"from LOCATIONS l inner join SERVERS s on l.server_id = s.identifier " +
			"where l.identifier = :identifier";
	
	@Override
	public Long insert(Location location) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("folder", location.getFolder());
		params.put("server_id", location.getServer().getIdentifier());
		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder locationId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_LOCATION, namedParams, locationId, new String[] {"IDENTIFIER"});
		
		location.setIdentifier(locationId.getKey().longValue());
		return location.getIdentifier();
	}

	@Override
	public Location getLocationById(Long identifier) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", identifier);
		List<Location> locations = namedParameterJdbcTmpl.query(SQL_GET_LOCATION_BY_ID, namedParams, new LocationMapper());
	
		Location result = null;
		if (locations != null && !locations.isEmpty()) {
			result = locations.get(0);
		}
		
		return result;
	}	
	
	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}

	
	private static final class LocationMapper implements ParameterizedRowMapper<Location> {

		@Override
		public Location mapRow(ResultSet rs, int numRows) throws SQLException {
			Location location = new Location();
			location.setIdentifier(rs.getLong("identifier"));
			location.setFolder(rs.getString("folder"));
			
			Server server = new Server();
			server.setIdentifier(rs.getLong("server_id"));
			server.setHostName(rs.getString("host_name"));
			server.setIpAddress(rs.getString("ip_address"));
			server.setName(rs.getString("name"));
			
			location.setServer(server);
			return location;
		}
		
	}
}
