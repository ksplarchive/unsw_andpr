package edu.unsw.andpr.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.SpecimenDao;
import edu.unsw.andpr.domain.Specimen;

public class JdbcSpecimenDao implements SpecimenDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
		
	private static final String SQL_GET_ID_BY_BARCODE = 
			"select coalesce((select IDENTIFIER from SPECIMENS where BARCODE = :barcode), -1)";
	
	private static final String SQL_INSERT_SPECIMEN =
			"insert into SPECIMENS values(default, :lineage, :class, :type, :barcode, :tissue_site, :tissue_side)";
	
	private static final String SQL_GET_SPECIMEN_BY_ID = 
			"select identifier, lineage, class, type, barcode, tissue_site, tissue_side from specimens where identifier = :identifier";
	
	@Override
	public Long getSpecimenIdByBarcode(String barcode) {
		SqlParameterSource namedParams = new MapSqlParameterSource("barcode", barcode);
		return namedParameterJdbcTmpl.queryForLong(SQL_GET_ID_BY_BARCODE, namedParams);
	}
	
	public Long insert(Specimen specimen) {		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("lineage", specimen.getLineage());
		params.put("class", specimen.getSpecimenClass());
		params.put("type", specimen.getSpecimenType());
		params.put("barcode", specimen.getBarcode());
		params.put("tissue_site", specimen.getTissueSite());
		params.put("tissue_side", specimen.getTissueSide());
		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder specimenId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_SPECIMEN, namedParams, specimenId, new String[] {"IDENTIFIER"});
		
		specimen.setIdentifier(specimenId.getKey().longValue());
		return specimenId.getKey().longValue();
	}
	
	@Override
	public Specimen getSpecimenById(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		List<Specimen> specimens = namedParameterJdbcTmpl.query(SQL_GET_SPECIMEN_BY_ID, namedParams, new SpecimenRowMapper());
		
		Specimen result = null;
		if (specimens != null && !specimens.isEmpty()) {
			result = specimens.get(0);
		}
		
		return result;
	}

	
	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}
	
	private static class SpecimenRowMapper implements ParameterizedRowMapper<Specimen> {
		@Override
		public Specimen mapRow(ResultSet rs, int numRows) throws SQLException {
			Specimen specimen = new Specimen();
			specimen.setIdentifier(rs.getLong("identifier"));
			specimen.setLineage(rs.getString("lineage"));
			specimen.setSpecimenClass(rs.getString("class"));
			specimen.setSpecimenType(rs.getString("type"));
			specimen.setBarcode(rs.getString("barcode"));
			specimen.setTissueSite(rs.getString("tissue_site"));
			specimen.setTissueSide(rs.getString("tissue_side"));
			return specimen;
		}		
	}

}
