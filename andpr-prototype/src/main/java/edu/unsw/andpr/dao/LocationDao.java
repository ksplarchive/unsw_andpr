/**
 * 
 */
package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.Location;

public interface LocationDao {
	public Long insert(Location location);
	
	public Location getLocationById(Long identifier);

}
