package edu.unsw.andpr.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.ServerDao;
import edu.unsw.andpr.domain.Server;

public class JdbcServerDao implements ServerDao {
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
	
	private static final String SQL_GET_SERVER_ID = 
			"select coalesce((select IDENTIFIER from SERVERS where NAME = :name), -1)";
	
	private static final String SQL_INSERT_SERVER = 
			"insert into SERVERS values(default, :host_name, :ip_address, :name)";


	@Override
	public Long getServerId(String name) {
		SqlParameterSource namedParams = new MapSqlParameterSource("name", name);
		return namedParameterJdbcTmpl.queryForLong(SQL_GET_SERVER_ID, namedParams);
	}
	
	public Long insert(Server server) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("host_name", server.getHostName());
		params.put("ip_address", server.getIpAddress());
		params.put("name", server.getName());
		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder serverId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_SERVER, namedParams, serverId, new String[] {"IDENTIFIER"});
		
		server.setIdentifier(serverId.getKey().longValue());
		return server.getIdentifier();		
	}

	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}
}
