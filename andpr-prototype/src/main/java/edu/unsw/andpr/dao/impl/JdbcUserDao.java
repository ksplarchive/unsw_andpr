package edu.unsw.andpr.dao.impl;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import edu.unsw.andpr.dao.UserDao;
import edu.unsw.andpr.domain.User;

public class JdbcUserDao implements UserDao {
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
	
	private static final String SQL_GET_BY_LOGIN_ID = 
			"select identifier, first_name, last_name, login_id, password from users " +
			"where login_id = :loginId";
	
	private static final String SQL_GET_BY_ID =
			"select identifier, first_name, last_name, login_id from users " +
			"where identifier = :identifier";
			

	@Override
	public User getUserByLoginId(String loginId) {
		SqlParameterSource namedParams = new MapSqlParameterSource("loginId", loginId);
		List<User> users = namedParameterJdbcTmpl.query(SQL_GET_BY_LOGIN_ID, namedParams, new UserMapper());
		User result = null;
		if (users != null && !users.isEmpty()) {
			result = users.get(0);
		}
		
		return result;
	}

	@Override
	public User getUserById(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		List<User> users = namedParameterJdbcTmpl.query(SQL_GET_BY_ID, namedParams, new UserWithoutPasswordMapper());
		
		User result = null;
		if (users != null && !users.isEmpty()) {
			result = users.get(0);
		}
		
		return result;
	}
	
	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);
	}

	private static final class UserMapper implements ParameterizedRowMapper<User> {
		@Override
		public User mapRow(ResultSet rs, int rowNum) 
		throws SQLException {
			User user = new User();
			user.setIdentifier(rs.getLong("identifier"));
			user.setFirstName(rs.getString("first_name"));
			user.setLastName(rs.getString("last_name"));
			user.setLoginId(rs.getString("login_id"));
			user.setPasswd(rs.getString("password"));
			return user;
		}
	}	
	
	private static final class UserWithoutPasswordMapper implements ParameterizedRowMapper<User> {
		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setIdentifier(rs.getLong("identifier"));
			user.setFirstName(rs.getString("first_name"));
			user.setLastName(rs.getString("last_name"));
			user.setLoginId(rs.getString("login_id"));
			return user;
		}
	}
}
