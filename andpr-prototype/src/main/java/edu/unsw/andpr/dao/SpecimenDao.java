/**
 * 
 */
package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.Specimen;

/**
 * @author Vinayak Pawar
 *
 */
public interface SpecimenDao {
	public Long getSpecimenIdByBarcode(String barcode);
	
	public Specimen getSpecimenById(Long id);
	
	public Long insert(Specimen specimen);
}
