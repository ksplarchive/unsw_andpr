package edu.unsw.andpr.dao;

import java.util.List;

import edu.unsw.andpr.domain.Image;
import edu.unsw.andpr.domain.ImageSummary;

public interface ImageDao {
	public Image getImageById(Long id);
	
	public Long insert(Image image);
	
	public List<ImageSummary> getImagesByParticipantName(String name, int startRow, int maxRows);
	
	public List<ImageSummary> getImagesBySpecimenBarcode(String barcode, int startRow, int maxRows);
	
	public List<ImageSummary> getImagesByInstrumentManufacturer(String manufacturer, int startRow, int maxRows);
	
	public List<ImageSummary> getImagesByTileCoordinates(int x, int y, int startRow, int maxRows);
	
	public List<ImageSummary> getImagesUploadedInLast(int nDays);
	
	public List<ImageSummary> getAllImages(int startRow, int maxRows);
	
	public int getImagesCount();
	
	public int getImagesCount(int nDays);
	
	public int getTiledImagesCount();
	
	public int getTiledImagesCount(int nDays);
}
