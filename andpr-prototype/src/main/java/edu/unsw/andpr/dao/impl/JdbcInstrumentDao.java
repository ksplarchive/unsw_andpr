package edu.unsw.andpr.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.InstrumentDao;
import edu.unsw.andpr.domain.Instrument;

public class JdbcInstrumentDao implements InstrumentDao {
	
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
	
	private static final String SQL_CHECK_INSTRUMENT_ID = 
			"select coalesce((select IDENTIFIER from INSTRUMENTS where IDENTIFIER = :identifier), -1)";
	
	private static final String SQL_INSERT_INSTRUMENT = 
			"insert into INSTRUMENTS values(default, :name, :manufacturer, :manufacturer_model_name, :software_version, :site) ";
	
	private static final String SQL_GET_INSTRUMENT_BY_ID =
			"select identifier, name, manufacturer, manufacturer_model_name, software_version, site from INSTRUMENTS where identifier = :identifier";
	
	@Override
	public boolean doesInstrumentExists(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		Long iid = namedParameterJdbcTmpl.queryForLong(SQL_CHECK_INSTRUMENT_ID, namedParams);
		return iid != -1;
	}
	
	@Override
	public Instrument getInstrumentById(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		List<Instrument> instruments = namedParameterJdbcTmpl.query(SQL_GET_INSTRUMENT_BY_ID, namedParams, new InstrumentMapper());
		
		Instrument result = null;
		if (instruments != null && !instruments.isEmpty()) {
			result = instruments.get(0);
		}
		
		return result;
	}
	

	@Override
	public Long insert(Instrument instrument) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", instrument.getName());
		params.put("manufacturer", instrument.getManufacturer());
		params.put("manufacturer_model_name", instrument.getModelName());
		params.put("software_version", instrument.getSoftwareVersion());
		params.put("site", instrument.getSite());
		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder instrumentId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_INSTRUMENT, namedParams, instrumentId, new String[] {"IDENTIFIER"});
		
		instrument.setIdentifier(instrumentId.getKey().longValue());
		return instrument.getIdentifier();		
	}	
	
	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}
	
	private static final class InstrumentMapper implements ParameterizedRowMapper<Instrument> {
		@Override
		public Instrument mapRow(ResultSet rs, int numRows) throws SQLException {
			Instrument instrument = new Instrument();
			instrument.setIdentifier(rs.getLong("identifier"));
			instrument.setName(rs.getString("name"));
			instrument.setManufacturer(rs.getString("manufacturer"));
			instrument.setModelName(rs.getString("manufacturer_model_name"));
			instrument.setSoftwareVersion(rs.getString("software_version"));
			instrument.setSite(rs.getString("site"));
			return instrument;
		}
		
	}
}
