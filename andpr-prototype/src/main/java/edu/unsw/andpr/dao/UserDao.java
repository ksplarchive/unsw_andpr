package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.User;

public interface UserDao {
	public User getUserByLoginId(String loginId);
	
	public User getUserById(Long id);
}
