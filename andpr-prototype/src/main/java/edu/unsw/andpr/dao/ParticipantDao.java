package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.Participant;

public interface ParticipantDao {
	public boolean doesParticipantExists(Long id);
	
	public Participant getParticipantById(Long id);
	
	public Long insert(Participant p);

}
