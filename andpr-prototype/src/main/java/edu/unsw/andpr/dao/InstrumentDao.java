package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.Instrument;

public interface InstrumentDao {
	public boolean doesInstrumentExists(Long id);
	
	public Instrument getInstrumentById(Long id);
	
	public Long insert(Instrument instrument);

}
