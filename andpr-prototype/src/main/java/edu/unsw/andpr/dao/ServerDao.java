package edu.unsw.andpr.dao;

import edu.unsw.andpr.domain.Server;

public interface ServerDao {
	public Long getServerId(String name);
	
	public Long insert(Server server);
}
