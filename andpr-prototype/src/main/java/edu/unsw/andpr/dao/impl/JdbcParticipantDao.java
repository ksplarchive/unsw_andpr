package edu.unsw.andpr.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.ParticipantDao;
import edu.unsw.andpr.domain.Participant;

public class JdbcParticipantDao implements ParticipantDao {
	
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
	
	private static final String SQL_CHECK_PARTICIPANT_ID = 
			"select coalesce((select IDENTIFIER from PARTICIPANTS where IDENTIFIER = :identifier), -1) from dual";

	private static final String SQL_INSERT_PARTICIPANT =
			"insert into PARTICIPANTS values (default, :first_name, :last_name, :date_of_birth)";
	
	private static final String SQL_PARTICIPANT_BY_ID =
			"select identifier, first_name, last_name, date_of_birth from PARTICIPANTS where identifier = :identifier";
	
	@Override
	public boolean doesParticipantExists(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		Long pid = namedParameterJdbcTmpl.queryForLong(SQL_CHECK_PARTICIPANT_ID, namedParams);
		return pid != -1;
	}

	@Override
	public Participant getParticipantById(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		List<Participant> participants = namedParameterJdbcTmpl.query(SQL_PARTICIPANT_BY_ID, namedParams, new ParticipantRowMapper());

		Participant result = null;
		if (participants != null && !participants.isEmpty()) {
			result = participants.get(0);
		}
		
		return result;
	}	
	
	@Override
	public Long insert(Participant p) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("first_name", p.getFirstName());
		params.put("last_name", p.getLastName());
		params.put("date_of_birth", p.getDateOfBirth());

		SqlParameterSource namedParams = new MapSqlParameterSource(params);		
		KeyHolder participantId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_PARTICIPANT, namedParams, participantId, new String[] {"IDENTIFIER"});
			
		p.setIdentifier(participantId.getKey().longValue());
		return participantId.getKey().longValue();
	}

	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}
	
	private static final class ParticipantRowMapper implements ParameterizedRowMapper<Participant> {
		@Override
		public Participant mapRow(ResultSet rs, int rowNum) throws SQLException {
			Participant participant = new Participant();
			participant.setIdentifier(rs.getLong("identifier"));
			participant.setFirstName(rs.getString("first_name"));
			participant.setLastName(rs.getString("last_name"));
			participant.setDateOfBirth(rs.getDate("date_of_birth"));
			return participant;
		}		
	}

}
