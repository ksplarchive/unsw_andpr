package edu.unsw.andpr.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.unsw.andpr.dao.ImageDao;
import edu.unsw.andpr.domain.Image;
import edu.unsw.andpr.domain.ImageSummary;
import edu.unsw.andpr.domain.Instrument;
import edu.unsw.andpr.domain.Location;
import edu.unsw.andpr.domain.Participant;
import edu.unsw.andpr.domain.Specimen;
import edu.unsw.andpr.domain.TiledImage;
import edu.unsw.andpr.domain.User;

public class JdbcImageDao implements ImageDao {
	private NamedParameterJdbcTemplate namedParameterJdbcTmpl;
	
	private static final String SQL_INSERT_IMAGE = 
			"insert into IMAGES values(" +
			"    default, :acquisition_date, :archived, :name, :description, " +
			"    :format, :file_size, :resolution, :label, :activity_status, :thumbnail, :created_by, :last_updated_by," +
			"    :specimen_id, :participant_id, :generated_by, :stored_at, :update_time)";
	
	private static final String SQL_INSERT_TILED_IMAGE =
			"insert into TILED_IMAGES values(" +
			"    default, :x, :y, :height, :width, :file_size, :resolution, :tiled_date, :format, " +
			"    :stored_at, :image_id, :tile_set_id, :update_time)";
	
	private static final String SQL_GET_IMAGE_BY_ID =
			"select identifier, acquisition_date, archived, name, description, format, " +
			"file_size, resolution, label, activity_status, created_by, last_updated_by, specimen_id, " +
			"participant_id, generated_by, stored_at, update_time from IMAGES where identifier = :identifier";
	
	private static final String SQL_GET_TILED_IMAGES_BY_IMAGE_ID =
			"select identifier, x, y, height, width, file_size, resolution, tiled_date, format, " +
			"stored_at, update_time from TILED_IMAGES where image_id = :image_id";

	private static final String SQL_GET_IMAGE_SEARCH_HEAD = 
			"select i.identifier, i.acquisition_date, i.name, i.format, " +
			"i.file_size, i.resolution, it.manufacturer, s.class from " +
			"IMAGES i inner join INSTRUMENTS it on it.identifier = i.generated_by " +
			"inner join SPECIMENS s on s.identifier = i.specimen_id ";
	
	private static final String SQL_GET_IMAGE_SEARCH_TAIL =
			"order by i.identifier limit :startRow, :endRow";

	private static final String SQL_GET_IMAGE_BY_PARTICIPANT_NAME =
			SQL_GET_IMAGE_SEARCH_HEAD + 
			" inner join PARTICIPANTS p on i.participant_id = p.identifier " +
			"where lower(p.first_name) like :name or lower(p.last_name) like :name " +
			SQL_GET_IMAGE_SEARCH_TAIL;
	
	private static final String SQL_GET_IMAGE_BY_SPECIMEN_BARCODE =
			SQL_GET_IMAGE_SEARCH_HEAD +
			"where s.barcode = :barcode " + 
			SQL_GET_IMAGE_SEARCH_TAIL;
	
	private static final String SQL_GET_IMAGE_BY_INSTRUMENT_MANF =
			SQL_GET_IMAGE_SEARCH_HEAD +
			"where lower(it.manufacturer) like :manufacturer " + 
			SQL_GET_IMAGE_SEARCH_TAIL;

	private static final String SQL_GET_IMAGE_BY_TILED_IMAGE_COORD =
			"select distinct i.identifier, i.acquisition_date, i.name, i.format, " +
			"i.file_size, i.resolution, it.manufacturer, s.class from  " +
			"IMAGES i inner join TILED_IMAGES ti on ti.image_id = i.identifier " +
			"inner join INSTRUMENTS it on it.identifier = i.generated_by " +
			"inner join SPECIMENS s on i.specimen_id = s.identifier " +
			"where ti.x <= :x and (ti.x + ti.width) > :x and ti.y <= :y and (ti.y + ti.height) > :y " +
			SQL_GET_IMAGE_SEARCH_TAIL;
	
	private static final String SQL_GET_ALL_IMAGES =
			SQL_GET_IMAGE_SEARCH_HEAD + " " + SQL_GET_IMAGE_SEARCH_TAIL;
	
	private static final String SQL_GET_IMAGES_UPLOADED_SINCE =
			SQL_GET_IMAGE_SEARCH_HEAD +
			"where i.update_time >= :time";
	
	private static final String SQL_GET_IMAGES_COUNT =
			"select count(*) from IMAGES";
	
	private static final String SQL_GET_IMAGES_UPLOADED_COUNT_SINCE =
			"select count(*) from IMAGES where update_time >= :time";

	private static final String SQL_GET_TILED_IMAGES_COUNT =
			"select count(*) from TILED_IMAGES";

	private static final String SQL_GET_TILED_IMAGES_UPLOADED_COUNT_SINCE =
			"select count(*) from TILED_IMAGES where update_time >= :time";

		
	@Override
	public Image getImageById(Long id) {
		SqlParameterSource namedParams = new MapSqlParameterSource("identifier", id);
		List<Image> images = namedParameterJdbcTmpl.query(SQL_GET_IMAGE_BY_ID, namedParams, new ImageMapper());

		Image result = null;
		if (images != null && !images.isEmpty()) {
			result = images.get(0);
			result.setTiledImages(new HashSet<TiledImage>(getTiledImagesByImageId(id)));
		}
		
		return result;
	}
	
	@Override
	public List<ImageSummary> getImagesByParticipantName(String name, int startRow, int maxRows) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", "%" + name.toLowerCase() + "%");
		params.put("startRow", startRow);
		params.put("endRow", startRow + maxRows);
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		return namedParameterJdbcTmpl.query(SQL_GET_IMAGE_BY_PARTICIPANT_NAME, namedParams, new ImageSummaryMapper());
	}	
	
	
	public List<TiledImage> getTiledImagesByImageId(Long imageId) {
		SqlParameterSource namedParams = new MapSqlParameterSource("image_id", imageId);
		return namedParameterJdbcTmpl.query(SQL_GET_TILED_IMAGES_BY_IMAGE_ID, namedParams, new TiledImageMapper());
	}
	
	@Override
	public List<ImageSummary> getImagesBySpecimenBarcode(String barcode, int startRow, int maxRows) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("barcode", barcode);
		params.put("startRow", startRow);
		params.put("endRow", startRow + maxRows);
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		return namedParameterJdbcTmpl.query(SQL_GET_IMAGE_BY_SPECIMEN_BARCODE, namedParams, new ImageSummaryMapper());
	}

	@Override
	public List<ImageSummary> getImagesByInstrumentManufacturer(String manufacturer, int startRow, int maxRows) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("manufacturer", "%" + manufacturer.toLowerCase() + "%");
		params.put("startRow", startRow);
		params.put("endRow", startRow + maxRows);
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		return namedParameterJdbcTmpl.query(SQL_GET_IMAGE_BY_INSTRUMENT_MANF, namedParams, new ImageSummaryMapper());
	}

	@Override
	public List<ImageSummary> getImagesByTileCoordinates(int x, int y, int startRow, int maxRows) {
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("x", x);
		params.put("y", y);
		params.put("startRow", startRow);
		params.put("endRow", startRow + maxRows);		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		return namedParameterJdbcTmpl.query(SQL_GET_IMAGE_BY_TILED_IMAGE_COORD, namedParams, new ImageSummaryMapper());
	}
	
	@Override
	public List<ImageSummary> getAllImages(int startRow, int maxRows) {
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("startRow", startRow);
		params.put("endRow", startRow + maxRows);		
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		return namedParameterJdbcTmpl.query(SQL_GET_ALL_IMAGES, namedParams, new ImageSummaryMapper());
	}
	
	
	@Override
	public List<ImageSummary> getImagesUploadedInLast(int nDays) {
		SqlParameterSource namedParams = new MapSqlParameterSource("time", getTime(nDays));
		return namedParameterJdbcTmpl.query(SQL_GET_IMAGES_UPLOADED_SINCE, namedParams, new ImageSummaryMapper());
	}

	
	@Override
	public int getImagesCount() {		
		SqlParameterSource params = null;
		return namedParameterJdbcTmpl.queryForInt(SQL_GET_IMAGES_COUNT, params);
	}

	@Override
	public int getImagesCount(int nDays) {
		SqlParameterSource params = new MapSqlParameterSource("time", getTime(nDays));
		return namedParameterJdbcTmpl.queryForInt(SQL_GET_IMAGES_UPLOADED_COUNT_SINCE, params);
	}

	@Override
	public int getTiledImagesCount() {
		SqlParameterSource params = null;
		return namedParameterJdbcTmpl.queryForInt(SQL_GET_TILED_IMAGES_COUNT, params);
	}

	@Override
	public int getTiledImagesCount(int nDays) {
		SqlParameterSource params = new MapSqlParameterSource("time", getTime(nDays));
		return namedParameterJdbcTmpl.queryForInt(SQL_GET_TILED_IMAGES_UPLOADED_COUNT_SINCE, params);
	}
		
	@Override
	public Long insert(Image image) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("acquisition_date", image.getAcquisitionDate());
		params.put("archived", image.isArchived() ? 1 : 0);
		params.put("name", image.getName());
		params.put("description", image.getDescription());
		params.put("format", image.getFormat());
		params.put("file_size", image.getFileSize());
		params.put("resolution", image.getResolution());
		params.put("label", image.getLabel());
		params.put("activity_status", image.getActivityStatus());
		params.put("thumbnail", null);
		params.put("created_by", image.getCreatedBy().getIdentifier());
		params.put("last_updated_by", image.getLastUpdatedBy().getIdentifier());
		
		if (image.getSpecimen() != null) {
			params.put("specimen_id", image.getSpecimen().getIdentifier());
		} else {
			params.put("specimen_id", null);
		}
		
		if (image.getParticipant() != null) {
			params.put("participant_id", image.getParticipant().getIdentifier());
		} else {
			params.put("participant_id", null);
		}
		
		params.put("generated_by", image.getGeneratedBy().getIdentifier());
		params.put("stored_at", image.getStoredAt().getIdentifier());
		params.put("update_time", image.getUpdateTime());
				
		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder imageId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_IMAGE, namedParams, imageId, new String[] {"IDENTIFIER"});
		
		image.setIdentifier(imageId.getKey().longValue());
		
		if (image.getTiledImages() != null) {
			for (TiledImage tiledImage : image.getTiledImages()) {
				tiledImage.setImage(image);
				insert(tiledImage);
			}			
		}
		
		return image.getIdentifier();
	}
	
	public Long insert(TiledImage tiledImage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("x", tiledImage.getX());
		params.put("y", tiledImage.getY());
		params.put("height", tiledImage.getHeight());
		params.put("width", tiledImage.getWidth());
		params.put("file_size", tiledImage.getFileSize());
		params.put("resolution", tiledImage.getResolution());
		params.put("tiled_date", tiledImage.getTiledDate());
		params.put("format", tiledImage.getFormat());
		params.put("stored_at", tiledImage.getLocation().getIdentifier());
		params.put("image_id", tiledImage.getImage().getIdentifier());
		params.put("tile_set_id", null);
		params.put("update_time", tiledImage.getUpdateTime());

		SqlParameterSource namedParams = new MapSqlParameterSource(params);
		KeyHolder tiledImageId = new GeneratedKeyHolder();
		namedParameterJdbcTmpl.update(SQL_INSERT_TILED_IMAGE, namedParams, tiledImageId, new String[] {"IDENTIFIER"});
		
		tiledImage.setIdentifier(tiledImageId.getKey().longValue());
		return tiledImage.getIdentifier();
	}
	
	public void setDataSource(final DataSource dataSource) {
		namedParameterJdbcTmpl = new NamedParameterJdbcTemplate(dataSource);		
	}
	
	private Date getTime(int nDays) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DAY_OF_MONTH, -nDays);
		
		return cal.getTime();
	}
	
	
	private static final class ImageSummaryMapper implements ParameterizedRowMapper<ImageSummary> {		
		@Override
		public ImageSummary mapRow(ResultSet rs, int rowNum)
		throws SQLException {
			ImageSummary image = new ImageSummary();
			image.setImageId(rs.getLong("identifier"));
			
			Date acquisitionDate = rs.getDate("acquisition_date");
			image.setAcquisitionDate(acquisitionDate != null ? acquisitionDate.getTime() : null);
			image.setName(rs.getString("name"));
			image.setFormat(rs.getString("format"));
			image.setFileSize(rs.getInt("file_size"));
			image.setResolution(rs.getString("resolution"));
			image.setEquipment(rs.getString("manufacturer"));
			image.setSpecimenClass(rs.getString("class"));
			return image;
		}		
	}
	
	private static final class ImageMapper implements ParameterizedRowMapper<Image> {

		@Override
		public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
			Image image = new Image();
			image.setIdentifier(rs.getLong("identifier"));
			image.setAcquisitionDate(rs.getDate("acquisition_date"));
			image.setArchived(rs.getBoolean("archived"));
			image.setName(rs.getString("name"));
			image.setDescription(rs.getString("description"));
			image.setFormat(rs.getString("format"));
			image.setFileSize(rs.getInt("file_size"));
			image.setResolution(rs.getString("resolution"));
			image.setLabel(rs.getString("label"));
			image.setActivityStatus(rs.getString("activity_status"));
			
			Long id = rs.getLong("created_by");
			User user = new User();
			user.setIdentifier(id);
			image.setCreatedBy(user);
			
			id = rs.getLong("last_updated_by");
			user = new User();
			user.setIdentifier(id);
			image.setLastUpdatedBy(user);
			
			id = rs.getLong("specimen_id");
			if (id != null) {
				Specimen specimen = new Specimen();
				specimen.setIdentifier(id);
				image.setSpecimen(specimen);
			}
			
			id = rs.getLong("participant_id");
			if (id != null) {
				Participant participant = new Participant();
				participant.setIdentifier(id);
				image.setParticipant(participant);
			}
			
			id = rs.getLong("generated_by");
			Instrument instrument = new Instrument();
			instrument.setIdentifier(id);
			image.setGeneratedBy(instrument);
			
			id = rs.getLong("stored_at");
			Location location = new Location();
			location.setIdentifier(id);
			image.setStoredAt(location);
			
			image.setUpdateTime(rs.getDate("update_time"));
			return image;
		}		
	}
	
	private static class TiledImageMapper implements ParameterizedRowMapper<TiledImage> {

		@Override
		public TiledImage mapRow(ResultSet rs, int rowNum) throws SQLException {
			TiledImage tiledImage = new TiledImage();
			tiledImage.setIdentifier(rs.getLong("identifier"));
			tiledImage.setX(rs.getInt("x"));
			tiledImage.setY(rs.getInt("y"));
			tiledImage.setHeight(rs.getInt("height"));
			tiledImage.setWidth(rs.getInt("width"));
			tiledImage.setFileSize(rs.getInt("file_size"));
			tiledImage.setResolution(rs.getString("resolution"));
			tiledImage.setTiledDate(rs.getDate("tiled_date"));
			tiledImage.setFormat(rs.getString("format"));
			
			Location location = new Location();
			location.setIdentifier(rs.getLong("stored_at"));
			tiledImage.setLocation(location);
			
			tiledImage.setUpdateTime(rs.getDate("update_time"));			
			return tiledImage;
		}		
	}
}
