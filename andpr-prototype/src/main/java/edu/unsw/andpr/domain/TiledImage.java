package edu.unsw.andpr.domain;

import java.util.Calendar;
import java.util.Date;

public class TiledImage {
	private Long identifier;
	
	private Integer x;
	
	private Integer y;
	
	private Integer height;
	
	private Integer width;
	
	private Integer fileSize;
	
	private String resolution;
	
	private Date tiledDate;
	
	private String format;
	
	private Location location;
	
	private TiledSet tiledSet;
	
	private Image image;
	
	private Date updateTime;

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public Date getTiledDate() {
		return tiledDate;
	}

	public void setTiledDate(Date tiledDate) {
		this.tiledDate = tiledDate;
	}
	
	public void setTiledDateInSecs(Long tiledDateInSecs) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(tiledDateInSecs * 1000);
		this.tiledDate = cal.getTime();
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public TiledSet getTiledSet() {
		return tiledSet;
	}

	public void setTiledSet(TiledSet tiledSet) {
		this.tiledSet = tiledSet;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
