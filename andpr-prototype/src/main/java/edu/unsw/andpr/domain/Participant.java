package edu.unsw.andpr.domain;

import java.util.Calendar;
import java.util.Date;

public class Participant {
	private Long identifier;
	
	private String firstName;
	
	private String lastName;
	
	private Date dateOfBirth;

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public void setDateOfBirthInSecs(Long dobInSecs) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(dobInSecs * 1000);
		this.dateOfBirth = cal.getTime();
	}
	
	

}
