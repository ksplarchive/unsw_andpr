package edu.unsw.andpr.domain;

public class Location {
	private Long identifier;
	
	private String folder;
	
	private Server server;

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}
	
	public void setServerName(String serverName) {
		server = new Server();
		server.setName(serverName);
	}
}
