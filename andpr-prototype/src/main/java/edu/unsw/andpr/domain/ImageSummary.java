package edu.unsw.andpr.domain;

public class ImageSummary {	
	private Long imageId;
	
	private Long acquisitionDate;
	
	private String name;
	
	private String format;
	
	private Integer fileSize;
	
	private String resolution;
	
	private String equipment;
	
	private String specimenClass;

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public Long getAcquisitionDate() {
		return acquisitionDate;
	}

	public void setAcquisitionDate(Long acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getSpecimenClass() {
		return specimenClass;
	}

	public void setSpecimenClass(String specimenClass) {
		this.specimenClass = specimenClass;
	}

}
