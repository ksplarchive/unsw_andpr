package edu.unsw.andpr.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Image {
	private Long identifier;
	
	private Date acquisitionDate;
	
	private boolean archived = false;
	
	private String name;
	
	private String description;
	
	private String format;
	
	private int fileSize;
	
	private String resolution;
	
	private String label;
	
	private String activityStatus;
	
	private User createdBy;
	
	private User lastUpdatedBy;
	
	private Specimen specimen;
	
	private Participant participant;
	
	private Instrument generatedBy;
	
	private Location storedAt;
	
	private Date updateTime;
	
	private Set<TiledSet> tiledSets = new HashSet<TiledSet>();
	
	private Set<TiledImage> tiledImages = new HashSet<TiledImage>();

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public Date getAcquisitionDate() {
		return acquisitionDate;
	}

	public void setAcquisitionDate(Date acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}
	
	public void setAcquisitionDateInSecs(Long acquisitionDateInSecs) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(acquisitionDateInSecs * 1000);
		this.acquisitionDate = cal.getTime();
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	
	public void setCreatedByUser(Long createdByUserId) {
		this.createdBy = new User();
		this.createdBy.setIdentifier(createdByUserId);
	}

	public User getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(User lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public void setLastUpdatedByUser(Long lastUpdatedByUserId) {
		this.lastUpdatedBy = new User();
		this.lastUpdatedBy.setIdentifier(lastUpdatedByUserId);
	}
	
	public Specimen getSpecimen() {
		return specimen;
	}

	public void setSpecimen(Specimen specimen) {
		this.specimen = specimen;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Instrument getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(Instrument generatedBy) {
		this.generatedBy = generatedBy;
	}
	
	public void setGeneratedByEquipment(Long instrumentId) {
		this.generatedBy = new Instrument();
		this.generatedBy.setIdentifier(instrumentId);
	}

	public Location getStoredAt() {
		return storedAt;
	}

	public void setStoredAt(Location storedAt) {
		this.storedAt = storedAt;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Set<TiledSet> getTiledSets() {
		return tiledSets;
	}

	public void setTiledSets(Set<TiledSet> tiledSets) {
		this.tiledSets = tiledSets;
	}

	public Set<TiledImage> getTiledImages() {
		return tiledImages;
	}

	public void setTiledImages(Set<TiledImage> tiledImages) {
		this.tiledImages = tiledImages;
	}	
	
	public void addTiledImage(TiledImage tiledImage) {
		tiledImages.add(tiledImage);
	}
}
