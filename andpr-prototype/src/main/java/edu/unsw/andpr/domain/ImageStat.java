/**
 * 
 */
package edu.unsw.andpr.domain;

/**
 * @author Vinayak Pawar
 *
 */
public class ImageStat {
	private int wsiCnt;
	
	private int tiledImagesCnt;
	
	private int lastWeekWsiUploadCnt;
	
	private int lastWeekTiledImagesUploadCnt;

	public int getWsiCnt() {
		return wsiCnt;
	}

	public void setWsiCnt(int wsiCnt) {
		this.wsiCnt = wsiCnt;
	}

	public int getTiledImagesCnt() {
		return tiledImagesCnt;
	}

	public void setTiledImagesCnt(int tiledImagesCnt) {
		this.tiledImagesCnt = tiledImagesCnt;
	}

	public int getLastWeekWsiUploadCnt() {
		return lastWeekWsiUploadCnt;
	}

	public void setLastWeekWsiUploadCnt(int lastWeekWsiUploadCnt) {
		this.lastWeekWsiUploadCnt = lastWeekWsiUploadCnt;
	}

	public int getLastWeekTiledImagesUploadCnt() {
		return lastWeekTiledImagesUploadCnt;
	}

	public void setLastWeekTiledImagesUploadCnt(int lastWeekTiledImagesUploadCnt) {
		this.lastWeekTiledImagesUploadCnt = lastWeekTiledImagesUploadCnt;
	}
}
