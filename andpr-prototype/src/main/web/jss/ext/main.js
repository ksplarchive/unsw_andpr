
<script>

//======================================Header Images View===========================================
var AppHeader = Backbone.View.extend({
	el:'.pageHeader',		
	render: function(){
		var template = _.template($('#header-template').html(),{});
		this.$el.html(template);		 
	}	
});
//======================================Login Page Blank Space View==================================
var LoginMenu = Backbone.View.extend({
	el:'.pageMenu',		
	render: function(){
		var template = _.template($('#login-menu-template').html(),{});
		this.$el.html(template);		 
	}	
});
//======================================Login Page Body View ========================================
var LoginPageBody = Backbone.View.extend({
	el:'.pageBody',
	
	events:{
		'submit .form-horizontal' : 'loginUser'		
	},
	
	loginUser:function(ev){
		//var loginDetails = $(ev.currentTarget).serializeObject();
		router.navigate('home',{trigger:true});		
 		return false;
	},
	
	render: function(){
		var template = _.template($('#loginPage-body-templ').html(),{});
		this.$el.html(template);		 
	}	
});

//======================================Home Page Menu View==========================================
var HomeMenu = Backbone.View.extend({
	el:'.pageMenu',		
	render: function(){
		var template = _.template($('#home-menu-template').html(),{});
		this.$el.html(template);		 
	}	
});

//======================================Home Page Body View---'Home'=================================
var HomeBody = Backbone.View.extend({
	el:'.pageBody',		
	render: function(){
		$("#homeBtn").toggleClass("btn-info");
		var template = _.template($('#home-body-template').html(),{});
		this.$el.html(template);		 
	}	
});

//======================================Home Page Body View---'UploadImage'==========================
var HomeBodyUpload = Backbone.View.extend({
	el:'.pageBody',		
	render: function(){
		$("#uploadBtn").toggleClass("btn-info");
		var template = _.template($('#home-upload-body-template').html(),{});
		this.$el.html(template);		 
	}	
});

//======================================Home Page Body View---'SearchImage'==========================
var HomeBodySearch = Backbone.View.extend({
	el:'.pageBody',		
	render: function(){
		$("#searchBtn").toggleClass("btn-info");
		var template = _.template($('#home-search-body-template').html(),{});
		this.$el.html(template);		 
	}	
});

//======================================Router=======================================================
var Router = Backbone.Router.extend({
	routes:{
		'':'LoginPage', 
		'home':'homePage',
		'upload':'uploadPage',
		'search':'searchPage'
	}
});
 
//======================================View Init====================================================
var appHeader = new AppHeader();

var loginMenu = new LoginMenu();
var loginPageBody = new LoginPageBody();

var homeMenu = new HomeMenu();
var homeBody = new HomeBody();

var homeBodyUpload = new HomeBodyUpload();

var homeBodySearch = new HomeBodySearch();

var router = new Router();
//======================================Router to render views====================================== 
router.on('route:LoginPage',function(){
	appHeader.render();
	loginMenu.render();
 	loginPageBody.render();
});

router.on('route:homePage',function(){
	appHeader.render();
	homeMenu.render();
	homeBody.render();
});

router.on('route:uploadPage',function(){
	appHeader.render();
	homeMenu.render();
	homeBodyUpload.render();
});

router.on('route:searchPage',function(){
	appHeader.render();
	homeMenu.render();
	homeBodySearch.render();
});

Backbone.history.start();
</script>
